---
repository: https://github.com/jfcmacro/commandlambdastream.git
story_name: Command Generic Story
story_language: java
tag: main-v2.0
origin: git@gitlab.com:jfcmacro/command-generic-story-java.git
---

# Command Generic Story

**Estimated reading time**: 30 minutes

## Story Outline

Programming languages were defined to help us turn a value into another value. The transformation is described as a well-formed complete program. That program is defined as a static program, which means if the computation is changed, the program must be rewritten to fit the new computation. This is common in almost all programming languages that belong to the Imperative Paradigm; the program must be rewritten. But, in paradigm declarative, the program is seen as data (a value) and can be transformed and rewritten dynamically as another computation.

Object-Oriented Programming, particularly Java, is an Imperative Programming Language, meaning that the computation described in a
source code is defined statically. Suppose you need to describe a calculation dynamically. In that case, you can use the Command Design Pattern: “Encapsulate a request as an object, thereby letting you parameterize clients with different request, queue or log request, and support undoable operations” [Gamma et al.,1995]. The Command Pattern can be used as parameterized computation, enabling us to dynamically change computation without recompiling the application.

In this story, we will use the Command Pattern to represent a dynamic computation through an application that implements the main functions of a database based on spreadsheets through the following types of computation: `forEach`, `filter`, `map`,  and `reduce`. This service will be implemented by starting with the design pattern called Command, as we already said it. In its first version, in a non-generic way, and the second we will improve it with
generic data type mechanisms.

## Story Organization
**Story Branch**: main

> `git checkout main`

**Practical task tag for self-study**: task

> `git checkout task`

Tags: #command_design_pattern, #generics

---
focus: src/main/java/com/epam/rd/cls/Main.java
---

### Command Generic Story

This story will tell a development, how to implement a dynamic computation using object-oriented programming and then, by using generics, improve the current code into another that gives us more flexibility.

Our story will start by showing how to implement an essential service of a table-based database similar to the one implemented in the spreadsheets through a service that will allow you to iterate in different ways:

* `forEach` applies a computation with side effects to each database item.
* `filter` through a predicate determines which elements are left in the database.
* `map` iterates over each item in the database producing a new base with a different type.
* `reduce` iterates over the database values producing a result.

We will implement these functions like this:

* Through the Command design pattern without generics.
* Through the Command design pattern with generics.

---
focus: src/main/java/com/epam/rd/cls/SalesSummaryRow.java
---

### Database Description

Our database design is based on a set of sales summarized on a table, where each row ([`SalesSummaryRow`](src/main/java/com/epam/rd/cls/SalesSummaryRow.java)) contains the sales of one article ([`Articles`](src/main/java/com/epam/rd/cls/Article.java)) on a region ([`Region`](src/main/java/com/epam/rd/cls/Region.java)) during a period. The database is generated through a helper class [`DBHelper`](src/main/java/com/epam/rd/cls/DBHelper.java) which returns a list of type values [`SalesSummaryRow`](src/main/java/com/epam/rd/cls/SalesSummaryRow.java), formally: `List<SalesSummaryRow>` is the type of our database data.

**Note:** Although we are already using generics with the type `List`, we do it because we do not want to add nongeneric code for the use of lists, bloating our unnecessary code, which is already more significant than we expect.

---
focus: src/main/java/com/epam/rd/cls/Command.java
---

### A Command Way

Each function described above (`forEach`, `filter`, `map`, `reduce`) describes a computation; the Interface [`Command`](src/main/java/com/epam/rd/cls/Command.java) is used to describe a computation. Therefore each of those functions can be defined by implementing its corresponding interface [`Command`](src/main/java/com/epam/rd/cls/Command.java).

A class that implements this interface [`Command`](src/main/java/com/epam/rd/cls/Command.java) defines a concrete computation. This computation is defined by a function that does not receive parameters or return a value, as shown in the next snippet code:

```java
public void function() {
...
}
```

A class that implements that interface [`Command`](src/main/java/com/epam/rd/cls/Command.java) enables us to define a function as data that can be passed as a parameter or returned as a result of computation; this allows us to define a behavior dynamically.

```java
Command command = new ClassImplementsCommand();
command.execute();
```

It can be passed as a parameter:

```java
public void apply(Command command) {
   command.execute();
}
```

It can be returned from a method:

```java
public Command getCommand(...) {
  ...
}
```

---
focus: src/main/java/com/epam/rd/cls/ForEachCmd.java
---

### The Service `forEach`

This service will iterate over each database element (table) and produce a computation with a side effect. That side effect could be an input and output operation or create a new database (table). The classes implementing the interface [`ForEachCmd`](src/main/java/com/epam/rd/cls/ForEachCmd.java) will traverse the database by applying a customized procedure. In this case, the process is defined by the interface [`SetSlsSumRwCmd`](src/main/java/com/epam/rd/cls/SetSlsSumRwCmd.java). This interface implements the interface [Command](src/main/java/com/epam/rd/cls/Command.java); it does this to receive a value of type [`SalesSummaryRow`](src/main/java/com/epam/rd/cls/SalesSummaryRow.java) in each evaluation.

The signature of the procedure is

```java
public void function(SalesSummaryRow salesSummaryRow) {
...
}
```

Because we have decided to avoid extensive using generics, we have to implement for each different type, be this primitive or object, a specific class that wrapper that type; for instance, for types: `Integer` and `Double`, we have defined each one as a concrete class: [`ForEachDblCmd`](src/main/java/com/epam/rd/cls/ForEachDblCmd.java)(`Double`), [`ForEachIntCmd`](src/main/java/com/epam/rd/cls/ForEachIntCmd.java) (`Integer`); both classes share the same code, the only thing that change is its inner type. 

We know that a better strategy is using generics. Still, in this particular case, we want to use the original definition of Command Design Pattern that does not use generics, and we have kept the same guidance for every has to do with Design Patterns; for the moment, the next step is using generics to avoid the current explosion of concrete classes.

---
focus: src/main/java/com/epam/rd/cls/FilterCmd.java
---

### The Service `filter`

The main idea of this service is to select elements of the database (Table) that meet a specific predicate (or criteria). The class [`FilterCmd`](src/main/java/com/epam/rd/cls/FilterCommand.java) defines this service. Its purpose is to produce a new database (Table). The [`FilterCmd`](src/main/java/com/epam/rd/cls/FilterCommand.java) extends the interface [`GetListSlsSumRwCmd`](src/main/java/com/epam/rd/cls/GetListSlsSumRwCmd.java), which in turn extends from [Command](src/main/java/com/epam/rd/cls/Command.java). The database (`List<SalesSummaryRow>`) and its predicate ([`GetBoolSetSlsSumRwCmd`](src/main/java/com/epam/rd/cls/GetBoolSetSlsRwCmd.java)) are received by its constructor.
The predicate implements the interface [`GetBoolSetSlsSumRwCmd`](src/main/java/com/epam/rd/cls/GetBoolSetSlsRwCmd.java); the interface takes any [`SalesSummaryRow`](src/main/java/com/epam/rd/cls/SalesSummaryRow.java) and returns a boolean value indicating whether that data row accomplishes a specific criterion or not. Below is the signature of the interface [`GetBoolSetSlsSumRwCmd`](src/main/java/com/epam/rd/cls/GetBoolSetSlsRwCmd.java):

```va
public boolean predicate(SalesSummaryRow salesSummaryRow) {
...
}
```

---
focus: src/main/java/com/epam/rd/cls/GetIntCmd.java
---

### The Service `map`

The main objective of the mapping service is to transform a type element [`SalesSummaryRow`](src/main/java/com/epam/rd/cls/SalesSummaryRow.java)
into any other type. For example, transform [`SalesSummaryRow`](src/main/java/com/epam/rd/cls/SalesSummaryRow.java) into a value of type integer (`int`). This would require a function that has the following signature:

```java
public int fromTo(SalesSummaryRow salesSummaryRow) {
...
}
```

This function will be applied to each element, producing a new value as shown in the previous example when it returns a value of type int. The class that contains this method (function) must implement the interface [`GetIntSetSlsSumRwCmd`](src/main/java/com/epam/rd/cls/GetIntSetSlsSumRwCmd.java); it is encharged to execute a function that receives a value of type [`SalesSummaryRow`](src/main/java/com/epam/rd/cls/SalesSummaryRow.java) and returns a value of type primitive integer (int); but, thanks to the java's boxing mechanism, it allows getting a value of type `Integer`.

The implementation will depend on the return type, as shown in [` MapListSlsSumRwIntCmd`](src/main/java/com/epam/rd/cls/MapListSlsSumRwIntCmd.java), which returns an `Integer` type, and [`MapListSlsSumRwDblCmd`](src/main/java/com/epam/rd/cls/MapListSlsSumRwDblCmd.java), which produces a `Double` type; if you want to return a different type, you must implement a new class that returns that type. You can see that the implementation walks through the database, applies the transform function `fromTo` to each element, and get the new database, as seen in the code [` MapListSlsSumRwIntCmd`](src/main/java/com/epam/rd/cls/MapListSlsSumRwIntCmd.java).

---
focus: src/main/java/com/epam/rd/cls/ReduceDblCmd.java
---

### The Service `reduce`

The purpose of this service is to condense the database into a value of a new kind. In this particular case, it will take the database from
Sales and condense it into a value of type `double`. It will be implemented as a service through the class [`ReduceDblCmd`](src/main/java/com/epam/rd/cls/ReduceDblCmd.java).

This service is carried out as they have been done with the rest services traversing the database, but in this case, an operator that has the following signature:


```java
public double operator(SalesSummaryRow salesSummaryRow,
                       double value) {
...
}
```

This is responsible for receiving an element from the database and a value of type `double` in the body of the function; it will take care of extracting or computing from this a value of type `double` and apply the operator to get a result. It will do it on each database element to finally reduce to a single value of type `double`.

The operator is implemented through the interface [`GetDblSetDblSetSlsSumRwCmd`](src/main/java/com/epam/rd/cls/GetDblSetDblSetSlsSumRwCmd.java).

Again, the database is traversed, applying this to each element of this operator, as seen in the implementation of the function [`execute`](src/main/java/com/epam/rd/cls/ReduceDoubleCommand.java:24-31).

The operator is implemented through the interface [`GetDblSetDblSetSlsSumRwCmd`](src/main/java/com/epam/rd/cls/GetDblSetDblSetSlsSumRwCmd.java). An important detail is observed in the constructor of [`ReduceDblCmd`](src/main/java/com/epam/rd/cls/ReduceDblCmd.java); the database and a reference to the operator are received, and the initial value is passed.

---
focus: src/main/java/com/epam/rd/cls/Main.java
---

### A Slight Improvement in the Functions: `forEach`, `filter`, `map`, `reduce`

Although these commands receive other commands as their parameters, their use requires a lot of repeated code (boilerplate code), knowing that the different implementations of said commands also have still more repeated code. Some functions have been made that simplify this repetition of code.

These are seen in the following:

* [`forEach`](src/main/java/com/epam/rd/cls/Main.java:7-12)
* [`filter`](src/main/java/com/epam/rd/cls/Main.java:28-35)
* [`map`](src/main/java/com/epam/rd/cls/Main.java:37-45)
* [`reduce`](src/main/java/com/epam/rd/cls/Main.java:57-67)

---
focus:
---

### Summary of "Command way"

Although we can implement the services on the database by extending the
use of the ` Command` design pattern, that pattern has several apparent problems:

   * Due to the lack of handling generality, there is a lot of repeated code.
   * Suppose a function requires a very high number of parameters. In that case, you will have to create many interfaces that support each parameter as the different type it has, therefore causing an explosion of interfaces in the case that they want to work with other types of data.
   * It is not flexible to return values, as the passed arguments require new interfaces that allow collecting those values.

Therefore, in the next part, we will add generality (generics) features to our `Command`.


---
tag: generics-v2.1
focus: src/main/java/com/epam/rd/cls/Command.java:old:3-5
---

### It Needs to Be More Generic. Oh, my Command, my Command!

We will start with the `Command` interface so that it can return values; for this, we have added the variable you type `R` to indicate that it will produce a value of any type.

---
focus: src/main/java/com/epam/rd/cls/SetCmd.java:5-7
---

### Passing Generics Arguments

Now, we can return a value from computation and set an argument. We achieve this by introducing the variable of type `T`, which indicates that it can receive any type. This can be dynamically input to the computation through the invocation of the `set` method.

A function that receives a `String` and converts it to an `Integer` could be implemented like this:

```java
SetCmd<String, Integer> sc = new SetCmd<String, Integer> {
   private String value;
   public void set(String value) {
     this.value = value;
   }
   public Integer execute() {
      return Integer.parseInt(value);
   }
}
```

To create, you can create instances of any type:

```java
SetCmd<String, Integer> sc = new SetCmd<>() {
   private String value;
   public void setValue(String value) {
      this.value = value;
   }
   public Integer execute() {
      return Integer.parseInt(value);
   }
}
```

Now, do we have a more flexible version? It makes a little more sense starting from a generic interface. We have infinite forms of representation, but what happens if the previous code returns a Double instead of returning an Integer? We have to rewrite some parts of the former code.


---
focus: src/main/java/com/epam/rd/cls/BiSetCmd.java
---

### More Parameters

What if I want more parameters?

There are two alternatives. The first is to pass an array of Objects and perform the conversion manually for each corresponding type, with the code repeated in each case.

The second manner is an intermediate way: creating a new generic interface for the number of arguments; those arguments are generic too. For instance, if we need two input arguments, we make an interface called `BiSetCmd<S, T, R>`, where `S` and `T` are input arguments, and `R` is the result. If do we need three input arguments? `TriSetCmd<P, S, T, R>`, where `P`, `S`, `T` are the input arguments, and `R` is the result. We use the same strategy for 4, 5, and so. What is the advantage of doing it in this manner? The compiler will check the implementation of that interface, and the type system will contain its corresponding parameters; we can be sure that the instance has parameters belonging to the same type. 

---
focus: src/main/java/com/epam/rd/cls/ForEachCmd.java
---

### `forEach` Generic Service

Having our generic base classes: [`Command`](src/main/java/com/epam/rd/cls/Command.java), [`SetCmd`](src/main/java/com/epam/rd/cls/SetCmd.java), and [`BiSetCmd`](src/main/java/com/epam/rd/cls/BiSetCmd.java), we can implement each of the base services of data generically.

We start with the class [`ForEachCmd`](src/main/java/com/epam/rd/cls/ForEachCmd.java). This class is parameterized in two elements: `T`, which represents the elements of the database, and `R`, which can be the value of return, although, for the proposed model, this last parameter is not required and may be instantiated to a value of type `Void`. `Void` is a class that represents an empty value.

The constructor of [`ForEachCmd`](src/main/java/com/epam/rd/cls/ForEachCmd.java) receives the database and the function (an instance of [`SetCmd`](src/main/java/com/epam/rd/cls/ForEachCmd.java)) representing a side effect computation. It can be seen as [`ForEachCmd`](src/main/java/com/epam/rd/cls/ForEachCmd.java) executed by iterating through each element, applying to each element of the database the function (or procedure if it returns an instance of `Void`) that function or method implements the interface [`ForEachCmd`](src/main/java/com/epam/rd/cls/ForEachCmd.java).

---
focus: src/main/java/com/epam/rd/cls/FilterCmd.java
---

### `filter` Generic Service

Now, let's look at the service implementation of the filter ([`FilterCmd`](src/main/java/com/epam/rd/cls/FilterCmd)).

The constructor [`FilterCmd`](src/main/java/com/epam/rd/cls/FilterCmd:10-14) is very similar to the above implementation of [`ForEachCmd`](src/main/java/com/epam/rd/cls/ForEachCmd.java), but in this case, the second generic argument of [`SetCmd`](src/main/java/com/epam/rd/cls/ForEachCmd.java) is concretized with a `Boolean` type. That represents a predicate that verifies if a value of type `T` accomplishes one specific criterion.

The following part shows the implementation of the method [`execute`](src/main/java/com/epam/rd/cls/FilterCmd:10-14), this function returns a new database with values from the same type, but that fulfills the predicate ([`SetCmd`](src/main/java/com/epam/rd/cls/SetCmd.java)).

---
focus: src/main/java/com/epam/rd/cls/MapCmd.java
---

###  `map` Generic Service

The map ([`MapCmd`](src/main/java/com/epam/rd/cls/MapCmd.java)) generic service implementation follows a similar line to the two previous generic solutions. The determining elements are the function represented by an instance of [`SetCmd`](src/main/java/com/epam/rd/cls/SetCmd.java) it is not the type is fixed, and the result is of interest since this will be part of the result that is a new database; we can observe this in the implementation of the method [`execute`](src/main/java/com/epam/rd/cls/MapCmd.java:17-24), which takes care of accumulating and adding each result to the return list (`retList`).

---
focus: src/main/java/com/epam/rd/cls/ReduceCmd.java
---

### `reduce` Generic Service

The reduce ([`ReduceCmd`](src/main/java/com/epam/rd/cls/ReduceCmd.java:12-18)) service has significant changes in the constructor; not only does it receive the database, but it also receives an instance of the [`BiSetCmd`](src/main/java/com/epam/rd/cls/BiSetCmd.java) interface that is the representation of an operator while receiving the default value of the operation. The implementation of the function [`execute`](src/main/java/com/epam/rd/cls/ReduceCmd.java:20-27) It can be seen how, through the [`BiSetCmd`](src/main/java/com/epam/rd/cls/BiSetCmd.java) type operator, it allows "accumulate" the result in each traversal on the database and at the end return said "accumulator".

---
focus: src/main/java/com/epam/rd/cls/Main.java
---

### Database Implementation with Generic Service

Let's look at how the database is implemented using the generic services.

* [`forEach`](src/main/java/com/epam/rd/cls/Main.java:7-12:old)
* [`filter`](src/main/java/com/epam/rd/cls/Main.java:28-35:old)
* [`map`](src/main/java/com/epam/rd/cls/Main.java:37-55:old)
* [`reduce`](src/main/java/com/epam/rd/cls/Main.java:57-67:old)

As we've already seen, the generic code service is more flexible when it is concretized with a specific type.

---
focus: src/main/java/com/epam/rd/cls/Main.java:51-60
---

### Implementation of Predicates

The construction of a predicate or any other computation is done by implementing the [`SetCmd`](src/main/java/com/epam/rd/cls/SetCmd.java) interface defining the types that go to work, either via defining an anonymous class or a class that implements that interface. For instance, the implementation of [`PredSouthCmd`](src/main/java/com/epam/rd/cls/PredSouthCmd.java) shows how it can be done. In the same way, the other necessary computations can be implemented following the same strategy on the defined services: `forEach`, `filter`, `map`, and `reduce`.

---
focus:
---

### Summary of a Generic Command Way

We have simplified the construction of computations by using generics. But even though classes have been reduced to implement the different ones, there are still problems:

* Precision, some implementations will require to be more verbose, although less than the non-generic ones, they are still very imprecise.
* Flexibility, since you have to use repeated code to define each computation.

The following section shows how to reduce these problems using lambda expressions.

---
focus:
---

### Conclusions

* We have shown a way of, like a fully oriented world to objects. We can use the characteristics in functional languages such as lambda,s expressions.
* Generality not only works in the early stages of object-oriented programming but is also the fundamental basis of programming with lambdas expressions.
* A language like Java is a language because of a large number of builders.
