# Command Generic Story

Command Generic Story is the source code to generate the Coding Story named: 
"Command Generic Story Java"

This tells a story about how to transform a Command Pattern into a 
Generic Version

This will translante by program [md2cs](https://github.com/jfcmacro/md2cs).

We assumed that you have installed the program [md2cs](https://github.com/jfcmacro/md2cs)

```shell
$ cd <dir where this repo is installed>
$ md2cs story.md
$ mv target/command-generic-story-java
$ git push
```

